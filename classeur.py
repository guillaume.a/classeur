#!/usr/bin/python3

# Testing: 
# $ python3 -m doctest -v classeur.py
# Security: 
#   This may be subject to denial of service (DOS) if reading untrusted files
#   due to vulnerabilities in Python's XML parser use for reading of PDF XML Metadata.
#   See https://docs.python.org/3/library/xml.html#xml-vulnerabilities

import datetime
import hashlib
import os
import re
import sys

import xml.parsers.expat
import PyPDF2

config_dedup = True

def get_createDate(path):
  with open(path, 'rb') as f:
    try:
      pdfr = PyPDF2.PdfFileReader(f, strict=False)
      try:
        pdfmeta = pdfr.getXmpMetadata()
        if pdfmeta != None:
          for key in ['xmp_create_date', 'xmp_modify_date']:
            try:
              dateAttr = getattr(pdfmeta, key)
              if isinstance(dateAttr, datetime.datetime):
                return dateAttr
            except (AttributeError, ValueError):
              # getattr may throw AttributeError
              # isinstance(..., datetime.datetime) may throw ValueError
              continue
      except (ValueError, TypeError, PyPDF2.errors.PdfReadError) as e:
        # getXmpMetadata() may raise exceptions for malformed metadata
        pass
      except xml.parsers.expat.ExpatError:
        # Workaround for PyPDF2 issue 585
        # https://github.com/mstamy2/PyPDF2/issues/585
        pass
      # If encrypted, try using an empty password
      if pdfr.isEncrypted:
        pdfr.decrypt("")
      pdfinfo = pdfr.getDocumentInfo()
      if pdfinfo != None:
        # Expected format: D:YYYYMMDDHHmmSSOHH'mm
        # cf Section 7.9.4 of "Document management — Portable document format — Part 1: PDF 1.7" 
        for key in ['/CreationDate', '/ModDate']:
          if key in pdfinfo:
            try:
              # Tolerate ' at any location
              datestr = pdfinfo[key].replace("'", "")
              # Tolerate missing leading D:
              if datestr[:2] == "D:":
                datestr = datestr[2:]
              # Tolerate "Z0000" instead of "Z" for Offset
              if datestr[-5:] == "Z0000":
                datestr = datestr[:-4]
              if len(datestr) == 14:
                return datetime.datetime.strptime(datestr, "%Y%m%d%H%M%S")
              elif len(datestr) > 14:
                return datetime.datetime.strptime(datestr, "%Y%m%d%H%M%S%z")
            except ValueError as e:
              continue
      else:
        return None
    except OSError as e:
      strerror = str(e)
      # PdfFileReader() may raise OSError with empty files
      return None
    except PyPDF2.errors.PdfReadError as e:
      # PdfFileReader() may raise PdfReadError with malformed PDFs
      return None

def normalize(filepath, filename):
  """
  Normalize a PDF document's filename

  :param filepath: str
  :param filename: str
  :return: str or None
  
  >>> normalize('./2001-01-01_notes', '2001-01-01_notes')
  '2001-01-01_notes'
  >>> normalize('./20010204notes', '20010204notes')
  '2001-02-04_notes'
  >>> normalize('./2001020447954123-capture', '2001020447954123-capture')
  None
  >>> normalize('./fact-2022-05-216-1937', 'fact-2022-05-216-1937')
  None
  """
  if re.match(r'^(20[0-9][0-9]-[0-1][0-9]-[0-3][0-9]_)(.*)', filename) != None:
    return filename
  
  name1 = re.sub(r'^(.*[^ _\-])[ _\-]+(20[0-9][0-9])[-_]([0-1][0-9])[-_]([0-3][0-9])([^0-9])', r'\2-\3-\4_\1\5', filename)
  name2 = re.sub(r'^20([0-9][0-9])([0-1][0-9])([0-3][0-9])([^0-9].*)' , r'20\1-\2-\3_\4', filename)
  name3 = re.sub(r'^(.*)[ _\-]([0-3][0-9])-([0-1][0-9])-([12][0-9][0-9][0-9])', r'\4-\3-\2_\1', filename)
  if filename != name1:
    return name1
  elif filename != name2:
    return name2
  elif filename != name3:
    return name3
  else:
    date = get_createDate(filepath)
    if date == None:
      return None
    return date.strftime('%Y-%m-%d') + '_' + filename

if __name__ == '__main__':
  if len(sys.argv) < 2:
    print("%s: missing path" % (sys.argv[0]))
    sys.exit(1)
  for (root, dirnames, filenames) in os.walk(sys.argv[1]):
    for filename in filenames:
      filepath = os.path.join(root, filename)
      newname = normalize(filepath, filename)
      if newname != None and filename != newname:
        newpath = os.path.join(root, newname)
        if os.path.exists(newpath):
          if config_dedup:
            f1 = open(filepath, 'rb')
            hashfunc = hashlib.sha256()
            hashfunc.update(f1.read())
            hash1 = hashfunc.digest()
            f2 = open(newpath, 'rb')
            hashfunc = hashlib.sha256()
            hashfunc.update(f2.read())
            hash2 = hashfunc.digest()
            if hash1 == hash2:
              print("%s -> %s (duplicate)" % (filename, newname))
              os.unlink(filepath)
            else:
              print("%s and %s are different" % (filename, newname))
          else:
            print("%s already exists" % (newname))
        else:
          print("%s -> %s" %(filename, newname))
          os.rename(filepath, newpath)
